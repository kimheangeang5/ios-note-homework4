//
//  LanguageManager.swift
//  Note-Homework4
//
//  Created by Kimheang on 12/11/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import Foundation
class LanguageManager{
    static var share = LanguageManager()
    var language: String {
        set{
            UserDefaults.standard.set(newValue, forKey: "langCode")
        }
        get{
            if let lang = UserDefaults.standard.string(forKey: "langCode") {
                return lang
            } else {
                return self.language
            }
        }
    }
}
extension String{
    var localized: String{
        get{
            let lang = LanguageManager.share.language
            let path = Bundle.main.path(forResource: lang, ofType: "lproj")
            let bundle = Bundle(path: path!)
            let translate = bundle?.localizedString(forKey: self, value: nil, table: nil)
            return translate!
        }
    }
}
