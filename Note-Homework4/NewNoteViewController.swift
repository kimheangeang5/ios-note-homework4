//
//  NewNoteViewController.swift
//  Note-Homework4
//
//  Created by Kimheang on 12/10/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import CoreData
class NewNoteViewController: UIViewController,UITextViewDelegate {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context: NSManagedObjectContext!
    @IBOutlet weak var fakeTextField: UITextView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var noteTextField: UITextField!
    var newNotes: NSManagedObject!
    var modifyNote: Notes!
    var frameRect : CGRect!
    var isUpdate = false
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        fakeTextField.delegate = self
        titleTextField.placeholder = "title".localized
        noteTextField.placeholder = "note".localized
        context = appDelegate.persistentContainer.viewContext
        if isUpdate{
            titleTextField.text = modifyNote.title
            fakeTextField.text = modifyNote.note
            if fakeTextField.text == "" {
                 noteTextField.placeholder = "note".localized
            }
            else{
                 noteTextField.placeholder = ""
            }
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        noteTextField.placeholder = ""
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if fakeTextField.text == "" {
            noteTextField.placeholder = "note".localized
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isUpdate{
            updateNote()
        }
        else{
            saveNote()
        }
    }
    
    func saveNote(){
        if fakeTextField.text == "" {
            fakeTextField.text = "Note"
        }
        newNotes = Notes(context: context)
        newNotes.setValue(titleTextField.text, forKey: "Title")
        newNotes.setValue(fakeTextField.text, forKey: "Note")
        appDelegate.saveContext()
    }
    
    func updateNote(){
        modifyNote.setValue(titleTextField.text, forKey: "Title")
        modifyNote.setValue(fakeTextField.text, forKey: "Note")
        appDelegate.saveContext()
    }
    
    @IBAction func addMore(_ sender: Any){
        let questionController = UIAlertController()
        questionController.addAction(UIAlertAction(title: "Take photo".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Choose Image".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Drawing".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Recording".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Checkboxs".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "CancelButton".localized, style: .cancel, handler: nil))
        self.present(questionController, animated: true, completion: nil)
    }
    @IBAction func more(_ sender: Any){
        let questionController = UIAlertController()
        questionController.addAction(UIAlertAction(title: "Delete".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Make a copy".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Send".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Collabrators".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "Labels".localized, style: .default, handler: nil))
        questionController.addAction(UIAlertAction(title: "CancelButton".localized, style: .cancel, handler: nil))
        self.present(questionController, animated: true, completion: nil)
    }
}
