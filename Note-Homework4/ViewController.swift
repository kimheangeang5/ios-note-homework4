//
//  ViewController.swift
//  Note-Homework4
//
//  Created by Kimheang on 12/10/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UICollectionViewDataSource {
    
    @IBOutlet weak var noteCollectionView: UICollectionView!
    @IBOutlet weak var takeNoteTextField: UITextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context: NSManagedObjectContext!
    var notes = [Notes]()
    var isLongPress = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        let layout = noteCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//
//        let leftAndRightPadding:CGFloat = 10
//        let numberOfItemsPerRow:CGFloat = 2
//        let itemWidth = (self.view.bounds.size.width - leftAndRightPadding) / numberOfItemsPerRow
//
//        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        self.takeNoteTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        context = appDelegate.persistentContainer.viewContext
        
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.3
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.noteCollectionView.addGestureRecognizer(lpgr)
        navigationController?.navigationBar.prefersLargeTitles = true
        // Add observer
        changeLanguaged()
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguaged), name: Notification.Name("languageChanged"), object: nil)
    }
    @objc func changeLanguaged(){
        takeNoteTextField.text = "takeNote".localized
        navigationItem.title = "headerTitle".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        notes = []
        getNotes()
        noteCollectionView.reloadData()
        
    }
    @IBAction func menuButtonTap(_ sender: Any) {
        showAddNoteScreen()
    }
    
    @objc func handleTap(){
        showAddNoteScreen()
    }
    
    func showAddNoteScreen(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addNewNote")
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getNotes(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for note in result as! [NSManagedObject] {
                notes.append(note as! Notes)
            }
        } catch {
            print("Failed")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NoteCollectionViewCell
        cell.titileLabel.text = notes[indexPath.row].title
        cell.noteLabel.text = notes[indexPath.row].note
        return cell
    }
    
    @IBAction func languageButtonChanged(_ sender: Any) {
        let questionController = UIAlertController()
        questionController.addAction(UIAlertAction(title: "KhmerButton".localized, style: .default, handler: { (_) in
            LanguageManager.share.language = "km"
            NotificationCenter.default.post(name: Notification.Name("languageChanged"), object: nil)
        }))
        questionController.addAction(UIAlertAction(title: "EnglishButton".localized, style: .default, handler: { (_) in
            LanguageManager.share.language = "en"
            NotificationCenter.default.post(name: Notification.Name("languageChanged"), object: nil)
        }))
        questionController.addAction(UIAlertAction(title: "CancelButton".localized, style: .cancel, handler: nil))
        self.present(questionController, animated: true, completion: nil)
    }
}

extension ViewController: UICollectionViewDelegate,UIGestureRecognizerDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let note = notes[indexPath.row]
        // send to updateViewController
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addNewNote") as! NewNoteViewController
        vc.modifyNote = note
        vc.isUpdate = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func deleteNote(indexPath: IndexPath){
        let questionController = UIAlertController(title: "Are you sure to delete this note?", message: nil, preferredStyle: .actionSheet)
        questionController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {
            (action:UIAlertAction!) -> Void in
            self.context.delete(self.notes[indexPath.row])
            self.notes.remove(at: indexPath.row)
            self.noteCollectionView.deleteItems(at: [indexPath])
            self.noteCollectionView.reloadData()
            self.appDelegate.saveContext()
        }))
        questionController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(questionController, animated: true, completion: nil)
    }
    
    
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        if (gestureRecognizer.state != UIGestureRecognizer.State.ended){
            return
        }
        let p = gestureRecognizer.location(in: self.noteCollectionView)
        if let indexPath : IndexPath = (self.noteCollectionView.indexPathForItem(at: p)){
            deleteNote(indexPath: indexPath)
        }
    }
}
