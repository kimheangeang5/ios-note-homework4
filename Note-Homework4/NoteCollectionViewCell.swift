//
//  NoteCollectionViewCell.swift
//  Note-Homework4
//
//  Created by Kimheang on 12/10/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
}
